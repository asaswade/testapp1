package com.example.test.testapp.ui.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.example.test.testapp.R;
import com.example.test.testapp.base.BaseFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class TestFragment extends BaseFragment {

    @BindView(R.id.txt_frag)
    TextView mTextView;
    private int num;


    public TestFragment() {
        // Required empty public constructor
    }

    public TestFragment(int num) {
            this.num = num;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_test, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    protected void initData() {

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mTextView.setText("Fragment number " + num);
    }

    @OnClick(R.id.frame_layout)
    public void onFrameLayoutClick(){
        showShortToast(mTextView.getText().toString());
    }
}
