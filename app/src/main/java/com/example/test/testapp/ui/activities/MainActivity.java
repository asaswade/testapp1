package com.example.test.testapp.ui.activities;

import android.graphics.Color;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.widget.TextView;

import com.example.test.testapp.R;
import com.example.test.testapp.adapter.ViewPagerAdapter;
import com.example.test.testapp.base.BaseActivity;
import com.example.test.testapp.ui.fragments.TestFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseActivity {
    @BindView(R.id.tabs)
    TabLayout mTabLayout;

    @BindView(R.id.viewpager)
    ViewPager mViewPager;

    @BindView(R.id.txt_pager_name)
    TextView mTextViewPagerName;

    @BindView(R.id.constraint_layout)
    ConstraintLayout mConstraintLayout;


    @Override
    protected void initData() {
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setupViewPager();
        tabSelectedListener();
    }

    /**
     * Tab Listener
     */
    private void tabSelectedListener() {
        mTextViewPagerName.setText("Item Tab 1");
        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mTextViewPagerName.setText(tab.getText().toString());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void setupViewPager() {

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        /*TestFragment testFragment= new TestFragment();
        testFragment.setArguments(setBundle(1));*/
        adapter.addFragment(new TestFragment(1), "Item Tab 1");
        adapter.addFragment(new TestFragment(2), "Item Tab 2");
        adapter.addFragment(new TestFragment(3), "Item Tab 3");
        adapter.addFragment(new TestFragment(4), "Item Tab 4");
        mViewPager.setAdapter(adapter);
        mTabLayout.setupWithViewPager(mViewPager);
    }


    @OnClick(R.id.btn_red)
    public void onRedClick(){
        mConstraintLayout.setBackgroundColor(Color.RED);
    }

    @OnClick(R.id.btn_blue)
    public void onBlueClick(){
        mConstraintLayout.setBackgroundColor(Color.BLUE);
    }

    @OnClick(R.id.btn_green)
    public void onGreenClick(){
        mConstraintLayout.setBackgroundColor(Color.GREEN);
    }

}
